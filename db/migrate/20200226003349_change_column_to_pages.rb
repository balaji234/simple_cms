class ChangeColumnToPages < ActiveRecord::Migration[6.0]
  def change
    change_column :pages, :permalink, :string
  end
end
